P.1: 	
		.data 0x10000000 		# Data declaration Section
		
msg1:	.asciiz "Please enter an integer between 1-9: " 
		.text					# Instruction Declaration Section
		.globl main  			# Tells the assembler that main will be accessible from outside the file
main:	addu $s0, $ra, $0 		# adds unsigned $ra and 0 together and stores in $s0 (saved temp 0): $s0 = $ra + 0
		
	# Print msg1 to screen
		li $v0, 4 	 			# loads opcode 4 (print str) into memory
		la $a0, msg1 	 		# load msg1 variable (a string) into memory
		syscall 				# Calls instruction in memory at $v0
		
	# Input integer and stores in $v0
		li $v0, 5				# loads opcode 5 (read integer) into memory
		syscall					# Calls instruction in memory at $v0
	
	# Store the input in $t0
		addu $t0, $v0, $0		# adds unsigned $v0 and 0 together in $t0 (temporary value 0) - t0 = v0 + 0
		
	# Shift the value in $t0 to the left by 7 (essentially multiplying it by 2^7 (128))
		sll $t0, $t0, 7			# use a shift left by 7 to multiply the number in $t0 by 2^7 or 128
	
	# Print the calulated integer
		li $v0, 1 				# loads opcode 1 (read integer) into memory
		addu $a0, $t0, $0 		# adds unsigned value in $t0 to 0 and stores in $a0
		syscall 				# Calls instruction in memory at $v0
		
	# End the program
		addu $ra, $0, $s0		# adds unsigned 0 and $s0 (first instruction in program stores to $s0)
		jr $ra 					# Jump register to return address ($r0)
