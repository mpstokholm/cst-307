# Michael Pedersen
# Date 9/1/2017
#
# 	This is my first program in 
#
#

# Data Declaration Section - Subsequent items are stored in the data segment
.data
	msg: .asciiz "Hello World\n"	# message to be printed - Hello World

# Instructions Section - Subsequent items are stored in the text
.text
	main:				# Main function section / start of code
		li $v0, 4		# Load instruction (opcode 4 - print string) into assembler temporary register
		la $a0, msg		# Load msg variable into $a0
		syscall			# Call instructions
	
	# End program 
		li $v0, 10		# Load instruction (opcode 10 - system exit) into assembler temporary register
		syscall			# Call instructions